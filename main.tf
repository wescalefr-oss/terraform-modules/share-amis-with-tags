data "aws_caller_identity" "source" {
  provider = aws.source
}

data "aws_ami" "ami" {
  owners = [data.aws_caller_identity.source.account_id]

  filter {
    name   = "image-id"
    values = [element(var.ami_ids, count.index)]
  }

  count = length(var.ami_ids)

  provider = aws.source
}

data "aws_caller_identity" "destination" {
  provider = aws.destination
}

resource "aws_ami_launch_permission" "sharing" {
  image_id   = element(var.ami_ids, count.index)
  account_id = data.aws_caller_identity.destination.account_id

  count = length(var.ami_ids)

  provider = aws.source
}

locals {
  tags = [
    for tags in(length(var.shared_tags) > 0 ? var.shared_tags : data.aws_ami.ami.*.tags) :
    templatefile("${path.module}/tags.tpl", {
      tags = [
        for key, value in tags :
        {
          key   = key
          value = value
        }
      ]
    })
  ]
}

resource "null_resource" "tags" {
  triggers = {
    resource_id = element(var.ami_ids, count.index)
    tags        = element(local.tags, count.index)
    profile     = var.profile
  }

  provisioner "local-exec" {
    when    = create
    command = "aws ec2 create-tags ${var.profile != "" ? "--profile ${var.profile}" : ""} --resources ${element(var.ami_ids, count.index)} --tags ${element(local.tags, count.index)}"
  }

  count = length(var.ami_ids)
}
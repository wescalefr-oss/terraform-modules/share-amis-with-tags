This project is archived, activity is being migrated to https://gitlab.com/wescalefr-oss/terraform-modules/terraform-aws-share-amis-with-tags for renaming purpose.       
Sources are kept for active stacks which would reference it. Please update your paths.

# Share AMIS with tags module
This module provides a way to share some AMIs with another account and copy tags (merging them with an input additional map) to the shared AMIs.    
It uses Terraform >= 0.12 for structure.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws.destination | n/a |
| aws.source | n/a |
| null | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| ami\_ids | List of the AMI ids the module will share | `list(string)` | n/a | yes |
| profile | An AWS profile to use to write the tags (leave empty for no profile) | `string` | `""` | no |
| shared\_tags | Tags of the shared AMIs by order (leave empty to reuse the source AMIs tags) | `list(map(string))` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| tags | List of maps of tags for each AMI found, can be used to produce shared\_tags input |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

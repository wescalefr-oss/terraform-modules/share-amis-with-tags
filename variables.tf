variable "ami_ids" {
  type        = list(string)
  description = "List of the AMI ids the module will share"
}

variable "profile" {
  type        = string
  description = "An AWS profile to use to write the tags (leave empty for no profile)"
  default     = ""
}

variable "shared_tags" {
  type        = list(map(string))
  description = "Tags of the shared AMIs by order (leave empty to reuse the source AMIs tags)"
  default     = []
}